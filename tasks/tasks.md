# Introduction

In this section, you will find a list of tasks that need to be completed. These tasks are essential for the project's progress and your active participation is crucial. Each task is outlined with detailed instructions to ensure that you can efficiently complete it. Please review and address each task diligently.

### Task 1: Know yourself

1. Add your entry to the **Meet the Team** section.
2. Arrange entries in alphabetical order of the first name.
3. Add your **"pang-malupitan"** image to the **team/images** directory with a file name like **bsis_2_visaya_christian** in **.jpg** format, using camel case for the file name.
4. Follow the template for other fields; make sure to fill out all of them.
5. Add your 16Personalities link. You can test it [here](https://www.16personalities.com/).
6. Create a new branch named like **bsis-2-visaya-christian**.
   - **Branch Naming Conventions**: Use the provided example format for branch naming.
7. Commit your changes, push them to the remote repository, and create a merge request (MR).
   - **Merge Request (MR) Guidelines**: Within the MR, please include an image illustrating the expected appearance of the changes.
8. The deadline for completing this task is September 14th at 7 am.

### Task 2: Pet the Python

1. Create a folder named `practice`.
2. Inside that folder, create a Python script named like `practice_python.py`
3. Complete the following tasks step by step, and make sure to write and run your Python code for each task to see the output.
4. Add appropriate comments to separate each tasks. For example:
    ```
    # Variable Assigments
    num1 = 1
    num2 = 2

    # Global and Local Variables
    global_variable = ''
    local_variable = ''
    ```
5. Save your work as you'll be submitting it using Git.

#### Variable Assignments
- **Task**: Create two variables, `num1` and `num2`, and assign them integer values. Then, calculate their sum and store it in a variable called `result`.
- **Task**: Assign a string value to a variable called `name`. Then, print a message using the `print()` function that says "Hello, [name]!" where `[name]` is replaced with the value stored in the `name` variable.

#### Global and Local Variables
- **Task**: Define a global variable `global_var` with a value of 10. Then, create a function `local_function` that defines a local variable `local_var` with a value of 5. Inside the function, print both `global_var` and `local_var`. Call the function and observe the output.
- **Task**: Create a function `modify_global` that attempts to change the value of `global_var` to 20. Call this function and print the value of `global_var` outside the function.

#### Arithmetic Operators
- **Task**: Create two variables named `num1` and `num2`, assign value to it. Write a function that takes two numbers from those variables and calculates their sum, difference, product, and quotient. Print the results.
- **Task**: Calculate the area of a triangle with a base of 8 units and a height of 6 units. Store the result in a variable called `area` and print it.

#### Augmented Assignment Operators
- **Task**: Initialize a variable `count` with the value 5. Use an augmented assignment operator to increment it by 3 and then multiply it by 2. Print the final value of `count`.
- **Task**: Create a variable `price` and set it to 100. Use an augmented assignment operator to decrease it by 20% to represent a discount. Print the discounted price.

#### Comparison Operators
- **Task**: Compare two numbers, `x` and `y` (assign value of your choice), to check if `x` is greater than `y`. Print the result.
- **Task**: Compare two strings, `str1` and `str2` (assign value of your choice), to check if they are equal (case-sensitive). Print the result.

#### In and Not In Operators
- **Task**: Create a list of fruits. Use the `in` operator to check if "apple" is in the list. Print the result.
- **Task**: Create a string `sentence` and use the `not in` operator to check if "world" is not in the sentence. Print the result.

#### List and List Manipulation
- **Task**: Create an empty list called `my_list`. Append three integers to it: 5, 10, and 15. Then, print the list.
- **Task**: Given a list of numbers, `numbers = [1, 2, 3, 4, 5]`, remove the last element using the appropriate list method. Print the modified list.

#### Sublist Using Slices
- **Task**: Create a list of numbers from 1 to 10. Use slicing to create a sublist containing elements from the 3rd to the 7th position (inclusive). Print the sublist.
- **Task**: Given a string `sentence` (assign value to it, make sure it has "quick" word), use slicing to extract the word "quick" from it and print it.

#### Getting the Index
- **Task**: Create a list of colors. Find and print the index of the color "green" in the list.
- **Task**: Create a list of fruits. Find and print the index of the color "mango" in the list.

#### Tuple
- **Task**: Create a tuple named `coordinates` containing latitude and longitude values. Attempt to change one of the values in the tuple.
- **Task**: Write a function (name is up to you) that returns a tuple containing the quotient and remainder when dividing two numbers. Call the function and print the result.

#### Dictionaries
- **Task**: Create a dictionary representing a person's information with keys like "name," "age," and "city." Print the person's name from the dictionary.
- **Task**: Edit person's city. Print the dictionary.

#### range() Function
- **Task**: Use the `range()` function to create a sequence of numbers from 1 to 10 (inclusive). Print the numbers.
- **Task**: Create a list of even numbers from 2 to 20 using the `range()` function. Print the list.

#### print() Function
- **Task**: Print the message "Hello, World!" using the `print()` function.
- **Task**: Create a list named `fruits` and assign values to it. Print the values of `fruits` separated by `,`.

#### input() Function
- **Task**: Write a program that takes the user's name as input and prints a personalized greeting message.
- **Task**: Ask the user for two numbers and calculate their product. Print the result.

#### len() Function
- **Task**: Create a list of your favorite books. Use the `len()` function to find and print the number of books in the list.
- **Task**: Get a user input sentence and calculate the number of characters in it using the `len()` function. Print the result.

#### continue / break Statements
- **Task**: Write a program that prints all the even numbers from 1 to 20 using a `for` loop. Use the `continue` statement to skip odd numbers.
- **Task**: Rewrite the same task above, but this time, if you reach 15, use `break` statement.

#### While and For Loops
- **Task**: Create a while loop that prints the numbers from 1 to 10.
- **Task**: Write a program that uses a `for` loop to iterate through a list of names and print each name with a greeting message.

#### Writing Functions
- **Task**: Write a function `add_numbers` that takes two numbers as input and returns their sum. Call the function and print the result.
- **Task**: Create a function `find_max` that takes a list of numbers as input and returns the maximum value in the list. Call the function and print the result.